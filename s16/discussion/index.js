// To check whether the script file is properly linked to your html file.
/*console.log("Hello Batch 288!");*/

// [Section] Arithmetic Operators
let x = 1397;
let y = 7831;

// Addition Operator(+)
let sum = x + y;

console.log("Result of addition operator: " + sum);

// Subtraction Operator (-)
let difference = y - x;
console.log("Result of subtraction operator: " + difference);

// Multiplication Operator (*)
let product = x * y;
console.log("Result of multiplication operator: " + product);
//Division Operator (/)
let quotient = y / x;
console.log("Result of division operator: " + quotient.toFixed(2));

// Modulo Operator (%)
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// [Section] Assignment Operators
// Assignment Operator (=)
// The assignment operator assigns/reassign the value to the variable.

let assignmentNumber = 8;

// Addition Assignment Operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable

assignmentNumber += 2;

console.log(assignmentNumber);

assignmentNumber += 3;
console.log("Result of addition assingment Operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator(-=, *=, /=)

// Subtraction Assigment Operator
assignmentNumber -= 2;
console.log("Result of subtraction assingment operator: " + assignmentNumber);

//Multiplication Assignment Operator
assignmentNumber *= 3;
console.log(
  "Result of multiplication assignment operator: " + assignmentNumber
);

// Division Assignment Operator
assignmentNumber /= 11;
console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses
// MDAS - Mutiplication or Division First then Addition or Subtraction, from left to right.
let mdas = 1 + 2 - (3 * 4) / 5;
/*
    1. 3 * 4 = 12
    2. 12 / 5 = 2.4
    1 + 2 - 2.4
    3. 1 + 2 = 3
    4. 3 - 2.4 = 0.6
*/

console.log(mdas.toFixed(1));

let pemdas = 1 + (2 - 3) * (4 / 5);
/*
    1. 4/5 = 0.8
    2. 2-3 = -1
    1 + (-1) * (0.8)
    3. -1 * 0.8 = -0.8
    4. 1 - 0.8
    0.2
*/

console.log(pemdas.toFixed(1));

//[Section] Increment and Decrement
//Operators that add or subtract values by 1 and reassings the value of the variable where the increment and decrement applied to.

let z = 1;

let increment = ++z;

console.log("Result of pre-increment: " + increment);

console.log("Result of pre-incerement: " + z);

increment = z++;
console.log("The result of post-increment: " + increment);

console.log("The result of post-increment: " + z);

x = 1;

let decrement = --x;

console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + x);

decrement = x--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + x);

// [Section] Type Coercion
/*
    -Type Coercion is the automatic or implicit conversion of values from one data type to another.
    -This happens when operations are performed on different data types that would normally possible and yield irregular results.
*/

let numA = "10";
let numB = 12;

let coercion = numA + numB;
// IF you are going to add string and number, it will concatenate its value
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

// the boolean "true" is also assciated with the value of 1
let numE = true + 1;
console.log(numE);

// the boolean "false" is also assciated with the value of 0
let numF = false + 1;
console.log(numF);

// [section] comparison operators

let juan = "juan";

// equality operator (==)

/*
 check whether  rhe operand are equal/have the same content/value.
 attemps to convert and compare operands of diffent data types.
 returns a boolaen value.
*/
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == "1"); //true
console.log(0 == false); //true
console.log("JUAN" == "juan"); //false (case-sensitive)

console.log(juan == "juan");

// inequality operator (!=)
/*
-check whether the operands are not equal/have different content/values.
-attemps to convert and compare operands of diffent data types
-returns to boolean value,
 */

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != "1");
console.log(0 != false);
console.log("JUAN" != "juan");
console.log(juan != "juan");

//strict equality operator (===)
/*
-checks whether the operands are equal/have the same content.
-also cmpare the data types of two values.
*/

console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === "1"); //false
console.log(0 === false); //false
console.log("JUAN" === "juan"); //false
console.log(juan === "juan"); //true

//strictly inequality operator
/*
-check whether the operands are not equal/have different content
-also compares the data types of 2 values
 */
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== "1"); //true
console.log(0 !== false); //true
console.log("JUAN" !== "juan"); //true
console.log(juan !== "juan"); //false

//[section] relational operators
// it checks whether one value is greater or less than to rhe other values.
let a = 50;
let b = 65;

// GT or greater than operator (>)
let isGreaterThan = a > b;
// LT or less than operator (<)
let isLessThan = a < b;
// GTE or Greater than or equal operator (>=)
let isGTorEqual = a >= b;
// LTE or less than or equal (<=)
let isLTorEqual = a <= b;

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

let numStr = "30";
console.log(a > numStr);
console.log(b <= numStr);

let strNum = "twenty"; //20
console.log(b >= strNum);

//[section] logical operator

let isLegalAge = true;
let isRegistered = false;

//logical AND operator (&& - double ampersand)
//returns true if all operands are true.
let allRequirementsMet = isLegalAge && isRegistered;

console.log("result of logical AND operator:" + allRequirementsMet);

//logical OR operator (|| - double pipe)
// returns true if one of the operands are true
// isLegalAge = false;
let someRequirementsMet = isLegalAge || isRegistered;
console.log("result of logical OR operator:" + someRequirementsMet);

//logical NOT Operator(! - Exclamation Point)
//returns the opposite value.
let someRequirementsNotMet = !isRegistered;
console.log("result of logical NOT operator:" + someRequirementsMet);
