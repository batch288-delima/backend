//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
// async function getSingleToDo(){

//     return await (

//        //add fetch here.

//        fetch('<urlSample>')
//        .then((response) => response.json())
//        .then((json) => json)

//    );

// }

// Getting all to do list item
async function getAllToDo() {
  return await //add fetch here.
  fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "GET",
    headers: {
      "Content-type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((json) => {
      return json.map((data) => data.title);
    });
}

// [Section] Getting a specific to do list item
async function getSpecificToDo() {
  return await //Add fetch here.
  fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "GET",
    headers: { "Content-type": "application/json" },
  })
    .then((response) => response.json())
    .then((json) => {
      return json;
    });
}

// [Section] Creating a to do list item using POST method
async function createToDo() {
  return await //Add fetch here.
  fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      title: "Code! Code! Code!",
      completed: false,
      userId: 1,
    }),
  })
    .then((response) => response.json())
    .then((json) => {
      return json;
    });
}

// [Section] Updating a to do list item using PUT method
async function updateToDo() {
  return await //Add fetch here.
  fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify({
      title: "Code! Code! Code!",
      description: "Coding is life",
      status: "pending",
      dateCompleted: "May 30, 2023",
      userId: 1,
    }),
  })
    .then((response) => response.json())
    .then((json) => {
      return json;
    });
}

// [Section] Deleting a to do list item
async function deleteToDo() {
  return await //Add fetch here.
  fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE",
    headers: { "Content-type": "application/json" },
  })
    .then((response) => response.json())
    .then((json) => {
      return json;
    });
}

//Do not modify
//For exporting to test.js
try {
  module.exports = {
    getSingleToDo: typeof getSingleToDo !== "undefined" ? getSingleToDo : null,
    getAllToDo: typeof getAllToDo !== "undefined" ? getAllToDo : null,
    getSpecificToDo:
      typeof getSpecificToDo !== "undefined" ? getSpecificToDo : null,
    createToDo: typeof createToDo !== "undefined" ? createToDo : null,
    updateToDo: typeof updateToDo !== "undefined" ? updateToDo : null,
    deleteToDo: typeof deleteToDo !== "undefined" ? deleteToDo : null,
  };
} catch (err) {}
