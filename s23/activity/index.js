// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names.
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added

// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method

// Create a constructor function called Pokemon for creating a pokemon

// Create/instantiate a new pokemon

// Create/instantiate a new pokemon

// Create/instantiate a new pokemon

// Invoke the tackle method and target a different object

// Invoke the tackle method and target a different object

let trainer = {
  name: "Ash Ketchum",
  age: "10",
  pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"],
  },
  talk: function (pokemonName) {
    if (trainer.pokemon.includes(pokemonName)) {
      return pokemonName + "! I choose you";
    } else {
      return "You don't have " + pokemonName + " in your list";
    }
  },
};

function Pokemon(pokemonName, pokemonLevel) {
  this.name = pokemonName;
  this.level = pokemonLevel;
  this.health = 2 * this.level;
  this.attack = 1 * this.level;

  this.tackle = function (target) {
    let remainingHealth = target.health - this.attack;
    console.log(this.name + " tackled " + target.name);
    if (remainingHealth <= 0) {
      return this.faint(target);
    } else {
      return target.name + "'s health is now reduced to " + remainingHealth;
    }
  };
  this.faint = function (target) {
    return target.name + " has fainted";
  };
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("MewTwo", 100);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    trainer: typeof trainer !== "undefined" ? trainer : null,
    Pokemon: typeof Pokemon !== "undefined" ? Pokemon : null,
  };
} catch (err) {}
